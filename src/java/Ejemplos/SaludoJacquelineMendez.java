//@author Jacqueline Mendez
package Ejemplos;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "Saludo", urlPatterns = {"/Saludo"})
public class SaludoJacquelineMendez extends HttpServlet
{
    protected void doGet(HttpServletRequest peticion, HttpServletResponse respuesta)
            throws ServletException, IOException
    {
        String nombre=peticion.getParameter("nombre");
        String aficion=peticion.getParameter("aficion");
        respuesta.setContentType("text/html;charset=UTF-8");
        PrintWriter salida=respuesta.getWriter();
        salida.println("<!DOCTYPE html>");
        salida.println("<head>");
        salida.println("<title>Procesamiento de peticiones con get con datos</title>");
        salida.println("</head>");
        salida.println("<body>");
        salida.println("<h1>hola:"+nombre+"<br/>");
        salida.println(" bienvenido a los servlets<br/>");
        salida.println(" Lo que mas le gusta hacer es:"+aficion+"<h1/>");
        salida.println("</body>");
        salida.println("</html>");
        salida.close();
    }
}
