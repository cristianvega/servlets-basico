package Ejemplos;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Cristian Vega
 */
@WebServlet(name = "ServletCuadratica", urlPatterns = {"/ServletCuadratica"})
public class CuadraticaCristianVega extends HttpServlet {

    protected void doPost(HttpServletRequest peticion, HttpServletResponse respuesta) 
        throws ServletException, IOException
            {
                String valora=peticion.getParameter("valora");
                String valorb=peticion.getParameter("valorb");   
                String valorc=peticion.getParameter("valorc");
                double v_a=Double.parseDouble(valora);
                double v_b=Double.parseDouble(valorb);
                double v_c=Double.parseDouble(valorc);
                double discriminante=(Math.pow(v_b,2)-4*(v_a*v_c)); 
                respuesta.setContentType("text/html");
                PrintWriter salida=respuesta.getWriter();
                salida.println("<head>");
                salida.println("<title> Procesamiento de peticiones post con datos</title>");
                salida.println("</head>");
                salida.println("<body>");
                salida.println("<h3>! valor discriminante: "+discriminante+"<br/>");
                if (v_a<=0)
                    {
                        salida.println("<h3>! El valor de A es menor o igual a CERO /no se puede dividir: "+v_a+"<br/>");
                    }
                else
                    {    
                    if (discriminante<0)
                        salida.println("<h3>! La funcion Cuadratica NO TIENE SOLUCION"+"<br/>");
                    if (discriminante==0.0)
                        salida.println("<h3>! X1 y X2 son iguales a:"+(-v_b/(2*v_a))+"<br/>");
                    if (discriminante>0)
                    {
                        salida.println("<h3>! El valor de X1 es:"+(-v_b+Math.sqrt(discriminante))/(2*v_a)+"<br/>");
                        salida.println("<h3>! El valor de X2 es:"+(-v_b-Math.sqrt(discriminante))/(2*v_a)+"<br/>");
                    }    
                    }    
                salida.println("</body>");
                salida.println("</html>");
                salida.close();
}

}
